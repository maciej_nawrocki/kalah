package com.nawrocki.kalah.game;

import com.nawrocki.kalah.game.domain.Game;
import com.nawrocki.kalah.game.persistance.KalahGameInMemoryRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class KalahGameServiceTest {

    KalahGameInMemoryRepository gameInMemoryRepository = new KalahGameInMemoryRepository();
    KalahGameService gameService = new KalahGameService(gameInMemoryRepository);

    @Test
    void createGameTest() {
        //given
        var numberOfGames = gameInMemoryRepository.findAll().size();

        //when
        var result = gameService.createGame();

        //then
        assertNotNull(result);
        assertSame(Game.class, result.getClass());
        assertEquals(numberOfGames + 1, gameInMemoryRepository.findAll().size());
    }

    @Test
    void makeMoveTest() {
        //given
        var game = gameInMemoryRepository.save(mock(Game.class));

        //when
        var result = gameService.makeMove(game.getId(), 1);

        //then
        verify(game, times(1)).makeMove(anyInt());
    }
}
