package com.nawrocki.kalah.game.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PitsTest {

    private static final int STARTING_NUMBER_OF_STONES = 6;

    private static Stream<Arguments> provideStringsForIsBlank() {
        return Stream.of(
                Arguments.of(1, Game.Player.PLAYER_ONE, STARTING_NUMBER_OF_STONES, false, 2),
                Arguments.of(2, Game.Player.PLAYER_ONE, STARTING_NUMBER_OF_STONES, false, 3),
                Arguments.of(3, Game.Player.PLAYER_ONE, STARTING_NUMBER_OF_STONES, false, 4),
                Arguments.of(4, Game.Player.PLAYER_ONE, STARTING_NUMBER_OF_STONES, false, 5),
                Arguments.of(5, Game.Player.PLAYER_ONE, STARTING_NUMBER_OF_STONES, false, 6),
                Arguments.of(6, Game.Player.PLAYER_ONE, STARTING_NUMBER_OF_STONES, false, 7),
                Arguments.of(7, Game.Player.PLAYER_ONE, 0, true, 8),
                Arguments.of(8, Game.Player.PLAYER_TWO, STARTING_NUMBER_OF_STONES, false, 9),
                Arguments.of(9, Game.Player.PLAYER_TWO, STARTING_NUMBER_OF_STONES, false, 10),
                Arguments.of(10, Game.Player.PLAYER_TWO, STARTING_NUMBER_OF_STONES, false, 11),
                Arguments.of(11, Game.Player.PLAYER_TWO, STARTING_NUMBER_OF_STONES, false, 12),
                Arguments.of(12, Game.Player.PLAYER_TWO, STARTING_NUMBER_OF_STONES, false, 13),
                Arguments.of(13, Game.Player.PLAYER_TWO, STARTING_NUMBER_OF_STONES, false, 14),
                Arguments.of(14, Game.Player.PLAYER_TWO, 0, true, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("provideStringsForIsBlank")
    void newPitsTest(int pitNumber, Game.Player owner, int startingNumberOfStones, boolean isKalah, int nextPitNumber) {
        //given

        //when
        var pits = Pits.newPits(startingNumberOfStones);

        //then
        assertEquals(owner, pits.get(pitNumber).owner());
        assertEquals(startingNumberOfStones, pits.get(pitNumber).getNumberOfStones());
        assertEquals(isKalah, pits.get(pitNumber).isKalah());
        assertEquals(pits.get(nextPitNumber), pits.get(pitNumber).getNext());
    }
}
