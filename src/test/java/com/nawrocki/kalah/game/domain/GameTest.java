package com.nawrocki.kalah.game.domain;

import com.nawrocki.kalah.game.domain.exception.MoveNotAllowedException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GameTest {

    @Test
    void newGameTest() {
        //given
        final var startingNumberOfStones = 6;

        //when
        var game = Game.newGame(startingNumberOfStones);

        //then
        assertNotNull(game);
        assertNotNull(game.getId());
        assertEquals(Game.Player.PLAYER_ONE, game.getTurn());
        assertEquals(startingNumberOfStones, game.getPits().get(1));
        assertEquals(startingNumberOfStones, game.getPits().get(2));
        assertEquals(startingNumberOfStones, game.getPits().get(3));
        assertEquals(startingNumberOfStones, game.getPits().get(4));
        assertEquals(startingNumberOfStones, game.getPits().get(5));
        assertEquals(startingNumberOfStones, game.getPits().get(6));
        assertEquals(0, game.getPits().get(7));
        assertEquals(startingNumberOfStones, game.getPits().get(8));
        assertEquals(startingNumberOfStones, game.getPits().get(9));
        assertEquals(startingNumberOfStones, game.getPits().get(10));
        assertEquals(startingNumberOfStones, game.getPits().get(11));
        assertEquals(startingNumberOfStones, game.getPits().get(12));
        assertEquals(startingNumberOfStones, game.getPits().get(13));
        assertEquals(0, game.getPits().get(14));
    }

    @Test
    void makeMoveTest() {
        //given
        final var startingNumberOfStones = 6;
        var game = Game.newGame(startingNumberOfStones);

        //when
        game.makeMove(2);

        //then
        assertEquals(Game.Player.PLAYER_TWO, game.getTurn());
        assertEquals(startingNumberOfStones, game.getPits().get(1));
        assertEquals(0, game.getPits().get(2));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(3));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(4));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(5));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(6));
        assertEquals(1, game.getPits().get(7));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(8));
        assertEquals(startingNumberOfStones, game.getPits().get(9));
        assertEquals(startingNumberOfStones, game.getPits().get(10));
        assertEquals(startingNumberOfStones, game.getPits().get(11));
        assertEquals(startingNumberOfStones, game.getPits().get(12));
        assertEquals(startingNumberOfStones, game.getPits().get(13));
        assertEquals(0, game.getPits().get(14));
    }

    @Test
    void cantMakeMoveOnKalahTurn() {
        //given
        final var startingNumberOfStones = 6;
        var game = Game.newGame(startingNumberOfStones);

        //when
        //then
        assertThrows(MoveNotAllowedException.class, () -> game.makeMove(7));
    }

    @Test
    void dontMakeMoveIfNotPlayerTurn() {
        //given
        final var startingNumberOfStones = 6;
        var game = Game.newGame(startingNumberOfStones);

        //when
        //then
        assertThrows(MoveNotAllowedException.class, () -> game.makeMove(8));
    }

    @Test
    void makeMoveAndDontChangeTurnWhenEndsInKalahTest() {
        //given
        final var startingNumberOfStones = 6;
        var game = Game.newGame(startingNumberOfStones);

        //when
        game.makeMove(1);

        //then
        assertEquals(Game.Player.PLAYER_ONE, game.getTurn());
        assertEquals(0, game.getPits().get(1));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(2));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(3));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(4));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(5));
        assertEquals(startingNumberOfStones + 1, game.getPits().get(6));
        assertEquals(1, game.getPits().get(7));
        assertEquals(startingNumberOfStones, game.getPits().get(8));
        assertEquals(startingNumberOfStones, game.getPits().get(9));
        assertEquals(startingNumberOfStones, game.getPits().get(10));
        assertEquals(startingNumberOfStones, game.getPits().get(11));
        assertEquals(startingNumberOfStones, game.getPits().get(12));
        assertEquals(startingNumberOfStones, game.getPits().get(13));
        assertEquals(0, game.getPits().get(14));
    }
}
