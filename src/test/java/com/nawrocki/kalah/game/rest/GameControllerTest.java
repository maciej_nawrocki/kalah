package com.nawrocki.kalah.game.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void canCreateGame() throws Exception {
        //given

        //when
        mockMvc.perform(post(GameController.GAMES))

                //then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.url").isNotEmpty());
    }

    @Test
    void canCheckStatusAfterGameCreated() throws Exception {
        //given
        var responseAsString = mockMvc.perform(post(GameController.GAMES)).andReturn().getResponse().getContentAsString();
        var gameId = new ObjectMapper().readTree(responseAsString).get("id").asText();

        //when
        mockMvc.perform(get(GameController.GAMES + '/' + gameId))

                //then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.url").isNotEmpty())
                .andExpect(jsonPath("$.status").isNotEmpty())
                .andExpect(jsonPath("$.status.1").isNotEmpty())
                .andExpect(jsonPath("$.status.1").value(6));
    }

    @Test
    void canMakeMoveAfterGameCreated() throws Exception {
        //given
        var responseAsString = mockMvc.perform(post(GameController.GAMES)).andReturn().getResponse().getContentAsString();
        var gameId = new ObjectMapper().readTree(responseAsString).get("id").asText();

        //when
        mockMvc.perform(put(GameController.GAMES + '/' + gameId + "/pits/1"))

                //then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.url").isNotEmpty())
                .andExpect(jsonPath("$.status").isNotEmpty())
                .andExpect(jsonPath("$.status.1").isNotEmpty())
                .andExpect(jsonPath("$.status.1").value(0));
    }
}