package com.nawrocki.kalah.infrastructure.exception.handler;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

@Getter
public class ApiErrorResponse {

    private final ZonedDateTime timestamp = ZonedDateTime.now();
    private final HttpStatus status;
    private final String error;
    private final String message;

    public ApiErrorResponse(HttpStatus status, String message) {
        this.status = status;
        this.error = status.getReasonPhrase();
        this.message = message;
    }
}
