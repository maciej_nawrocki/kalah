package com.nawrocki.kalah.infrastructure.exception.handler;

import lombok.val;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.annotation.Priority;

@ControllerAdvice
@Priority(Ordered.LOWEST_PRECEDENCE)
public class DefaultControllerExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorResponse> exception(Exception exception) {
        val response = new ApiErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error:" +  exception.getMessage());

        return new ResponseEntity<>(response, response.getStatus());
    }
}