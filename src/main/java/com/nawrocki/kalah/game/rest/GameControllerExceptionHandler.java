package com.nawrocki.kalah.game.rest;

import com.nawrocki.kalah.game.domain.exception.GameNotFoundException;
import com.nawrocki.kalah.game.domain.exception.MoveNotAllowedException;
import com.nawrocki.kalah.infrastructure.exception.handler.ApiErrorResponse;
import lombok.val;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.annotation.Priority;

@ControllerAdvice
@Priority(Ordered.HIGHEST_PRECEDENCE)
public class GameControllerExceptionHandler {

    @ExceptionHandler(GameNotFoundException.class)
    public ResponseEntity<ApiErrorResponse> gameNotFoundException(GameNotFoundException exception) {
        val response = new ApiErrorResponse(HttpStatus.NOT_FOUND, exception.getMessage());

        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(MoveNotAllowedException.class)
    public ResponseEntity<ApiErrorResponse> moveNotAllowedException(MoveNotAllowedException exception) {
        val response = new ApiErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage());

        return new ResponseEntity<>(response, response.getStatus());
    }
}