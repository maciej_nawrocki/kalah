package com.nawrocki.kalah.game.rest;

import com.nawrocki.kalah.game.domain.service.GameService;
import com.nawrocki.kalah.game.domain.Game;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(GameController.GAMES)
@RequiredArgsConstructor
class GameController {

    public static final String GAMES = "/games";
    private final GameService gameService;

    private static String gameLocation(Game game) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(GAMES)
                .path("/{id}")
                .buildAndExpand(game.getId())
                .toString();
    }

    @PostMapping
    ResponseEntity<CreateGameResponse> createGame() {
        val game = gameService.createGame();
        return ResponseEntity.ok().body(CreateGameResponse.of(game));
    }

    @GetMapping("{id}")
    ResponseEntity<GameStatusResponse> checkGame(@PathVariable UUID id) {
        val game = gameService.findGame(id);
        return ResponseEntity.ok(GameStatusResponse.of(game));
    }

    @PutMapping("{id}/pits/{pitId}")
    ResponseEntity<GameStatusResponse> makeMove(@PathVariable UUID id, @PathVariable int pitId) {
        val game = gameService.makeMove(id, pitId);
        return ResponseEntity.ok(GameStatusResponse.of(game));
    }

    @Getter
    @AllArgsConstructor
    static class CreateGameResponse {
        private final UUID id;
        private final String url;

        private static CreateGameResponse of(Game game) {
            return new CreateGameResponse(game.getId(), gameLocation(game));
        }
    }

    @Getter
    @AllArgsConstructor
    static class GameStatusResponse {
        private final UUID id;
        private final String url;
        private final Map<Integer, Integer> status;

        static GameStatusResponse of(Game game) {
            return new GameStatusResponse(game.getId(), gameLocation(game), game.getPits());
        }
    }
}
