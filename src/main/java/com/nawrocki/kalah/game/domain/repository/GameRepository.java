package com.nawrocki.kalah.game.domain.repository;

import com.nawrocki.kalah.game.domain.Game;

import java.util.UUID;

public interface GameRepository {

    Game save(Game game);

    Game findById(UUID id);
}
