package com.nawrocki.kalah.game.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.val;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.nawrocki.kalah.game.domain.Game.NUMBER_OF_ALL_PITS_INCLUDING_KALAH;
import static com.nawrocki.kalah.game.domain.Game.NUMBER_OF_PITS_PER_PLAYER;
import static com.nawrocki.kalah.game.domain.Game.NUMBER_OF_PITS_PER_PLAYER_INCLUDING_KALAH;
import static com.nawrocki.kalah.game.domain.Game.NUMBER_OF_PLAYERS;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Pits {

    @Getter
    private final LinkedHashMap<Integer, Pit> pits = new LinkedHashMap<>();
    private Pit head;
    private Pit tail;

    static Pits newPits(int startingNumberOfStones) {
        val pits = new Pits();
        var index = 1;
        for (int j = 0; j < NUMBER_OF_PLAYERS; j++) {
            for (int i = 0; i < NUMBER_OF_PITS_PER_PLAYER; i++) {
                pits.addPit(new Pit(index, false, startingNumberOfStones));
                index++;
            }
            pits.addPit(new Pit(index, true, 0));
            index++;
        }
        return pits;
    }

    public static Pits fromMap(Map<Integer, Integer> map) {
        val pits = new Pits();
        for (int i = 1; i <= NUMBER_OF_ALL_PITS_INCLUDING_KALAH; i++) {
            pits.addPit(new Pit(i, i % NUMBER_OF_PITS_PER_PLAYER_INCLUDING_KALAH == 0, map.get(i)));
        }
        return pits;
    }

    private void addPit(Pit newPit) {
        if (head == null) {
            head = newPit;
        } else {
            tail.setNext(newPit);
        }

        tail = newPit;
        tail.setNext(head);
        pits.putIfAbsent(newPit.getId(), newPit);
    }

    public Pit get(int id) {
        return pits.get(id);
    }

    public Map<Integer, Integer> toMap() {
        return pits.values()
                .stream()
                .collect(Collectors.toMap(Pit::getId, Pit::getNumberOfStones));
    }
}
