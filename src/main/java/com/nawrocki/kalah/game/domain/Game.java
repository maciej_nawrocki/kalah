package com.nawrocki.kalah.game.domain;

import com.nawrocki.kalah.game.domain.exception.MoveNotAllowedException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;

import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
public class Game {

    static final int NUMBER_OF_PLAYERS = 2;
    static final int NUMBER_OF_PITS_PER_PLAYER = 6;
    static final int NUMBER_OF_PITS_PER_PLAYER_INCLUDING_KALAH = 7;
    static final int NUMBER_OF_ALL_PITS_INCLUDING_KALAH = 14;

    @Getter
    private final UUID id;

    @Getter
    private Player turn;
    private final Pits pits;

    public static Game newGame(int startingNumberOfStones) {
        return new Game(UUID.randomUUID(), Player.PLAYER_ONE, Pits.newPits(startingNumberOfStones));
    }

    public Map<Integer, Integer> getPits(){
        return pits.toMap();
    }

    public void makeMove(int pitId) {
        val pit = pits.get(pitId);
        validate(pit);

        val stones = pit.pickAllStones();
        Pit currentPit = propagateStonesIntoNextPitsAndGetLast(pit, stones);
        changeTurnIfLastPitIsNotKalah(currentPit);
    }

    private void changeTurnIfLastPitIsNotKalah(Pit currentPit) {
        if (!currentPit.isKalah() || ! currentPit.owner().equals(turn)) {
            this.turn = turn.otherPlayer();
        }
    }

    private Pit propagateStonesIntoNextPitsAndGetLast(Pit pit, int stones) {
        var currentPit = pit;
        for (int i = stones; i > 0; i--) {
            currentPit = currentPit.getNext();
            currentPit.addStone();
        }
        return currentPit;
    }

    private void validate(Pit pit) {
        if (pit.isKalah()) {
            throw new MoveNotAllowedException("You cannot make a move on Kalah!");
        }
        if (!pit.owner().equals(turn)) {
            throw new MoveNotAllowedException("It's " + turn + " turn, you cannot make a move on pit number " + pit.getId());
        }
        if (pit.getNumberOfStones() < 1) {
            throw new MoveNotAllowedException("You cannot make a move on an empty Pit!");
        }
    }

    public enum Player {
        PLAYER_ONE,
        PLAYER_TWO;

        Player otherPlayer() {
            if (equals(PLAYER_ONE)) {
                return PLAYER_TWO;
            }
            return PLAYER_ONE;
        }
    }
}
