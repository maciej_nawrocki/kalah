package com.nawrocki.kalah.game.domain.service;

import com.nawrocki.kalah.game.domain.Game;

import java.util.UUID;

public interface GameService {

    Game createGame();

    Game findGame(UUID id);

    Game makeMove(UUID id, int pitId);
}
