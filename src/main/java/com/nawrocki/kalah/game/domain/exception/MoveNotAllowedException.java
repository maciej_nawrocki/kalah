package com.nawrocki.kalah.game.domain.exception;

public class MoveNotAllowedException extends RuntimeException {
    public MoveNotAllowedException(String message) {
        super(message);
    }
}
