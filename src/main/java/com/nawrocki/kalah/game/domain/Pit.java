package com.nawrocki.kalah.game.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.val;

import static com.nawrocki.kalah.game.domain.Game.NUMBER_OF_PITS_PER_PLAYER_INCLUDING_KALAH;

@Getter
@RequiredArgsConstructor
public class Pit {

    private final Integer id;
    private final boolean kalah;
    @NonNull
    private Integer numberOfStones;

    @Setter
    private Pit next;

    void addStone() {
        this.numberOfStones++;
    }

    Game.Player owner(){
        if (id <= NUMBER_OF_PITS_PER_PLAYER_INCLUDING_KALAH){
            return Game.Player.PLAYER_ONE;
        }
        return Game.Player.PLAYER_TWO;
    }

    int pickAllStones() {
        val numberOfStonesTmp = this.numberOfStones;
        this.numberOfStones = 0;
        return numberOfStonesTmp;
    }
}
