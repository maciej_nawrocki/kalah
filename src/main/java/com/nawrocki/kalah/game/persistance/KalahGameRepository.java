package com.nawrocki.kalah.game.persistance;

import com.nawrocki.kalah.game.domain.Game;
import com.nawrocki.kalah.game.domain.exception.GameNotFoundException;
import com.nawrocki.kalah.game.domain.repository.GameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class KalahGameRepository implements GameRepository {

    private final KalahGameSpringRepository kalahSpringRepository;

    @Override
    public Game save(Game game) {
        return kalahSpringRepository.save(GameEntity.of(game))
                .toGame();
    }

    @Override
    public Game findById(UUID id) {
        return kalahSpringRepository.findById(id)
                .orElseThrow(() -> new GameNotFoundException("Game with Id: " + id + " does not exist"))
                .toGame();
    }

}