package com.nawrocki.kalah.game.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface KalahGameSpringRepository extends CrudRepository<GameEntity, UUID> {}
