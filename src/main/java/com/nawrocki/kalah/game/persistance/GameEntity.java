package com.nawrocki.kalah.game.persistance;

import com.nawrocki.kalah.game.domain.Game;
import com.nawrocki.kalah.game.domain.Pits;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
class GameEntity {

    @Id
    private UUID id;

    private Game.Player turn;

    @ElementCollection
    private Map<Integer, Integer> pits;

    public static GameEntity of(Game game) {
        return new GameEntity(game.getId(), game.getTurn(), game.getPits());
    }

    public Game toGame() {
        return new Game(id, turn, Pits.fromMap(pits));
    }
}
