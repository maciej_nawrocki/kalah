package com.nawrocki.kalah.game.persistance;

import com.nawrocki.kalah.game.domain.Game;
import com.nawrocki.kalah.game.domain.exception.GameNotFoundException;
import com.nawrocki.kalah.game.domain.repository.GameRepository;
import lombok.val;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class KalahGameInMemoryRepository implements GameRepository {

    Map<UUID, Game> games = new HashMap<>();

    @Override
    public Game save(Game game) {
        games.putIfAbsent(game.getId(), game);
        return game;
    }

    @Override
    public Game findById(UUID id) {
        val game = games.get(id);
        if (game == null) {
            throw new GameNotFoundException("Game with Id: " + id + " does not exist");
        }
        return game;
    }

    public Collection<Game> findAll() {
        return games.values();
    }
}
