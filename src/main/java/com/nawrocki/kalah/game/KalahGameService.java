package com.nawrocki.kalah.game;

import com.nawrocki.kalah.game.domain.Game;
import com.nawrocki.kalah.game.domain.repository.GameRepository;
import com.nawrocki.kalah.game.domain.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class KalahGameService implements GameService {

    static final int STARTING_NUMBER_OF_STONES = 6;

    private final GameRepository gameRepository;

    @Override
    @Transactional
    public Game createGame() {
        return gameRepository.save(Game.newGame(STARTING_NUMBER_OF_STONES));
    }

    @Override
    public Game findGame(UUID id) {
        return gameRepository.findById(id);
    }

    @Override
    @Transactional
    public Game makeMove(UUID id, int pitId) {
        val game = gameRepository.findById(id);
        game.makeMove(pitId);
        return gameRepository.save(game);
    }
}
