# Kalah - Coding challenge

## General info
Technical Assignment for Backbase.

Kalah Game implementation.

## Technologies
Project is created with:
* Java 16
* Spring Boot 2.5.2
* H2 File Database


## Setup
To run this project, use command:
```
./gradlew bootRun
```

# Endpoints

You can access an automated documentation at:

* http://localhost:8080/swagger-ui.html#/
